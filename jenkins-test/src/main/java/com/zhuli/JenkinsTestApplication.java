package com.zhuli;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootApplication
@RestController
public class JenkinsTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(JenkinsTestApplication.class, args);
    }

    @RequestMapping("/")
    public Object test(){
        List list=new ArrayList<>();
        Map map=new HashMap<>();
        map.put("Total",0);
        map.put("AggregateResults",null);
        map.put("Errors",null);
        map.put("ExtendData",null);
        map.put("Data",list);
       return map;
    }

}
